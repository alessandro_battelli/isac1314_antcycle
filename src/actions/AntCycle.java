package it.unibo.alchemist.model.implementations.actions;


import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

import org.apache.commons.math3.analysis.interpolation.LinearInterpolator;
import org.apache.commons.math3.analysis.interpolation.SplineInterpolator;
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction;

import it.unibo.alchemist.external.cern.jet.random.engine.RandomEngine;
import it.unibo.alchemist.model.implementations.molecules.LsaMolecule;
import it.unibo.alchemist.model.interfaces.IEnvironment;
import it.unibo.alchemist.model.interfaces.ILsaMolecule;
import it.unibo.alchemist.model.interfaces.ILsaNode;
import it.unibo.alchemist.model.interfaces.INode;

public abstract class AntCycle extends LsaRandomNeighborAction {

	/**
	 * 
	 */
	private final boolean checkBatteryLevel;
	private boolean fixedFrequency;
	private static final long serialVersionUID = -9187179792212477488L;
	private int stepTime;
	private boolean a;
	private double eAwake, eSleep, rMin, rMax, s, r, g, threshold, pMin, pMax, sa, battery;
	private IEnvironment<List<? extends ILsaMolecule>> env;


	public AntCycle(final ILsaNode node, final ILsaMolecule molecule, final IEnvironment<List<? extends ILsaMolecule>> env, final double freq){
		this(node, molecule, env, freq, false);
	}

	public AntCycle(final ILsaNode node, final ILsaMolecule molecule, final IEnvironment<List<? extends ILsaMolecule>> env, final double freq, final boolean checkBatteryLevel) {
		this(node, molecule, env, null, 0.001, 0.0002, 0.16, 0.16, 0.0375, 1e-16, 0.00001, 0.001, 0.01, checkBatteryLevel);
		fixedFrequency = true;
		this.env = env;
		double[] xVals = new double[]{2,8,10,11,22,23,34,35,46,47,49,55,57,63,64,70,75,76,77,78,83,92,94};
		double[] yVals = new double[]{2.5e-7, 3.75e-7, 5e-7, 1e-6, 1.75e-6, 2.5e-6, 3.75e-6, 5e-6, 6.25e-6, 7.5e-6, 8.75e-6, 1e-5, 1.25e-5, 1.5e-5, 1.75e-5, 2e-5, 3.5e-5, 5e-5, 7.5e-5, 1e-4, 5e-4, 1e-3, 5e-3};

		//		SplineInterpolator interp = new SplineInterpolator();
		//		PolynomialSplineFunction myFunc = interp.interpolate(xVals, yVals);
		LinearInterpolator interp = new LinearInterpolator();
		PolynomialSplineFunction myFunc = interp.interpolate(xVals, yVals);

		pMin = myFunc.value(freq);
		pMax = pMin * 100;
	}

	public AntCycle(final ILsaNode node, final ILsaMolecule molecule, final IEnvironment<List<? extends ILsaMolecule>> env) {
		this(node, molecule, env, null, 0.001, 0.0002, 0.16, 0.16, 0.0375, 1e-16, 0.00001, 0.001, 0.01, true);
	}

	public AntCycle(final ILsaNode node, final ILsaMolecule molecule, final IEnvironment<List<? extends ILsaMolecule>> env, final RandomEngine r,
			final double awakeCons, final double sleepCons, final double minRad, final double maxRad,
			final double gain, final double thres, final double minProb, final double maxProb,
			final double sActiv, final boolean checkBatteryLevel) {
		super(node, molecule, env, r);
		this.checkBatteryLevel = checkBatteryLevel;
		fixedFrequency = false;
		stepTime = 0;
		a = false;
		s = 0;
		battery = 1;
		eAwake = awakeCons;
		eSleep = sleepCons;
		rMin = minRad;
		rMax = maxRad;
		this.r = computeR();
		g = gain;
		threshold = thres;
		pMin = minProb;
		pMax = maxProb;
		sa = sActiv;

	}

	private double computeAdaptiveRadius(){
		ILsaNode thisNode = getNode();
		double curRad = 0.0;
		int neighNum = 7;
		double minDistance = Double.MAX_VALUE;
		for(INode<List<? extends ILsaMolecule>> n1 : env.getNodes()){
			for(INode<List<? extends ILsaMolecule>> n2 : env.getNodes()){
				if(n1 != n2){
					double distance = env.getDistanceBetweenNodes(n1, n2);
					if(distance < minDistance)
						minDistance = distance;
				}
			}
		}

		curRad = minDistance;
		int neighSize = env.getNodesWithinRange(thisNode, curRad).size();
		while(neighSize < neighNum ||
				neighSize > neighNum + 3){
			if(neighSize > neighNum + 3)
				curRad /= 1.5;
			else
				curRad *= 1.2;
			neighSize = env.getNodesWithinRange(thisNode, curRad).size();
		}

		return curRad;
	}

	@Override
	public void execute() {	

		//calcolo il nuovo livello tenendo conto
		// dell'energia ricavata dall'ambiente
		double newBatteryLevel = battery + harvest();
		newBatteryLevel = newBatteryLevel > 1? 1 : newBatteryLevel;

		if(a)
			newBatteryLevel = newBatteryLevel - awakeConsumption();
		else
			newBatteryLevel = newBatteryLevel - sleepConsumption();

		newBatteryLevel = newBatteryLevel < 0? 0 : newBatteryLevel;
		battery = newBatteryLevel;

		//aggiorno la molecola
		ILsaMolecule bMol = new LsaMolecule("battery, N");
		ILsaNode thisNode = getNode();
		List<ILsaMolecule> listBMol = thisNode.getConcentration(bMol);
		for(ILsaMolecule mol : listBMol){
			thisNode.removeConcentration(mol);
		}
		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
		otherSymbols.setDecimalSeparator('.');
		DecimalFormat df = new DecimalFormat("0.00000", otherSymbols);
		thisNode.setConcentration(new LsaMolecule("battery, "+ df.format(battery)));
		// calcolo s nuovo
		s = computeS();

		if(battery > 0.05 || !checkBatteryLevel){
			//calcolo a
			a = computeA();

			//se il sensore è spento ha una probabilità di accendersi
			if(!a){
				double random = Math.random();
				double pActivate = pMin * (1 - battery) + pMax * battery;
				if(random < pActivate){
					s = sa;
					a = true;
				}
			}

			setActiveConcentration(a);

			//calcolo il nuovo raggio in base al nuovo valore di batteria
			if(fixedFrequency && stepTime == 0){
				r = computeAdaptiveRadius();
				ILsaMolecule rMol = new LsaMolecule("radius, N");
				List<ILsaMolecule> listRMol = thisNode.getConcentration(rMol);
				for(ILsaMolecule mol : listRMol){
					thisNode.removeConcentration(mol);
				}
				thisNode.setConcentration(new LsaMolecule("radius, "+ df.format(r)));	
			} else if(!fixedFrequency){
				r = computeR();
				ILsaMolecule rMol = new LsaMolecule("radius, N");
				List<ILsaMolecule> listRMol = thisNode.getConcentration(rMol);
				for(ILsaMolecule mol : listRMol){
					thisNode.removeConcentration(mol);
				}
				thisNode.setConcentration(new LsaMolecule("radius, "+ df.format(r)));	
			}



			//diffondo s ai vicini in base al raggio del passo precedente

			final List<? extends ILsaNode> nodes = getNodes();

			if(nodes!=null && nodes.size() > 0){
				for (final ILsaNode node : nodes) {
					setStateConcentration(node);
				}
			}


		} else {
			a = false;
			setActiveConcentration(a);

			r = 0;

			ILsaMolecule rMol = new LsaMolecule("radius, N");
			List<ILsaMolecule> listRMol = thisNode.getConcentration(rMol);
			for(ILsaMolecule mol : listRMol){
				thisNode.removeConcentration(mol);
			}
			thisNode.setConcentration(new LsaMolecule("radius, "+ df.format(r)));
		}

		if(a)
			behaviour();

		stepTime++; 
	}

	protected abstract void behaviour();

	private boolean computeA() {
		if(s > threshold)
			return true;
		else
			return false;
	}

	private double computeS() {
		ILsaMolecule sMol = new LsaMolecule("state, N");
		ILsaNode thisNode = getNode();
		List<ILsaMolecule> listSMol = thisNode.getConcentration(sMol);
		double sumS = 0;
		for(ILsaMolecule mol : listSMol){
			thisNode.removeConcentration(mol);
			sumS += Double.parseDouble(mol.getArg(1).toString());
		}
		double h = s + sumS;
		return Math.tanh(g*h);
	}

	private double computeR(){
		return rMin * (1-battery) + rMax * battery;
	}

	private double sleepConsumption() {
		return eSleep;
	}

	private double awakeConsumption() {
		return eAwake * (1+r);
	}

	private double harvest(){
		int sT = stepTime % 1440;
		double val = sT+0.5;
		if(sT < 420 || sT > 1140){
			return 0;
		} else {
			return 0.0027 * (1- Math.cos((val-420) * Math.PI /360))/2;
		}
	}


	protected void setStateConcentration(final ILsaNode node) {
		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
		otherSymbols.setDecimalSeparator('.');
		DecimalFormat df = new DecimalFormat("0.000000000000", otherSymbols);
		node.setConcentration(new LsaMolecule("state, "+ df.format(s)));
	}

	protected void setActiveConcentration(boolean active){
		ILsaMolecule aMol = new LsaMolecule("active, N");
		ILsaNode thisNode = getNode();
		List<ILsaMolecule> listAMol = thisNode.getConcentration(aMol);
		for(ILsaMolecule mol : listAMol){
			thisNode.removeConcentration(mol);
		}
		if(active)
			thisNode.setConcentration(new LsaMolecule("active, true"));
		else
			thisNode.setConcentration(new LsaMolecule("active, false"));
	}

	protected void setConcentration(final ILsaMolecule mol, final ILsaNode node){
		node.setConcentration(mol);
	}

	protected void removeConcentration(final ILsaMolecule mol, final ILsaNode node){
		if(node.getConcentration(mol).size() > 0){
			node.removeConcentration(mol);
		}
	}

}
