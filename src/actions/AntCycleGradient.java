package it.unibo.alchemist.model.implementations.actions;

import it.unibo.alchemist.external.cern.jet.random.engine.RandomEngine;
import it.unibo.alchemist.model.implementations.molecules.LsaMolecule;
import it.unibo.alchemist.model.interfaces.IEnvironment;
import it.unibo.alchemist.model.interfaces.ILsaMolecule;
import it.unibo.alchemist.model.interfaces.ILsaNode;

import java.util.List;

public class AntCycleGradient extends AntCycle {

	public AntCycleGradient(ILsaNode node, ILsaMolecule molecule,
			IEnvironment<List<? extends ILsaMolecule>> env,
			double freq) {
		super(node, molecule, env, freq);
		// TODO Auto-generated constructor stub
	}
	
	public AntCycleGradient(ILsaNode node, ILsaMolecule molecule,
			IEnvironment<List<? extends ILsaMolecule>> env,
			double freq, boolean checkBatteryLevel) {
		super(node, molecule, env, freq, checkBatteryLevel);
		// TODO Auto-generated constructor stub
	}
	
	public AntCycleGradient(ILsaNode node, ILsaMolecule molecule,
			IEnvironment<List<? extends ILsaMolecule>> env, RandomEngine r,
			double awakeCons, double sleepCons, double minRad, double maxRad,
			double gain, double thres, double minProb, double maxProb,
			double sActiv, boolean checkBatteryLevel) {
		super(node, molecule, env, r, awakeCons, sleepCons, minRad, maxRad, gain,
				thres, minProb, maxProb, sActiv, checkBatteryLevel);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected void behaviour(){
		
		ILsaMolecule tk = new LsaMolecule("token, N");
		
		List<ILsaMolecule> listTk = getNode().getConcentration(tk);
		
		if(listTk.size() > 0){
			
			int minVal = Integer.MAX_VALUE;
//			ILsaMolecule minMol = null;
			for(ILsaMolecule mol: listTk){
				int curVal = Integer.parseInt(mol.getArg(1).toString());
				if(minVal > curVal){
//					minMol = mol;
					minVal = curVal;
				}
			}
			
//			for(ILsaMolecule mol: listTk){
//				if(mol != minMol)
//					removeConcentration(mol,getNode());
//			}
			
			int diffuseVal = minVal+1;
			
			ILsaMolecule diffMol = new LsaMolecule("token,"+diffuseVal);
			
			for(ILsaNode node: getNodes()){
				setConcentration(diffMol, node);
			}
			
		}
		
		
	}

}
