package it.unibo.alchemist.model.implementations.actions;

import it.unibo.alchemist.external.cern.jet.random.engine.RandomEngine;
import it.unibo.alchemist.model.interfaces.IEnvironment;
import it.unibo.alchemist.model.interfaces.ILsaMolecule;
import it.unibo.alchemist.model.interfaces.ILsaNode;

import java.util.List;

public class AntCycleVoid extends AntCycle {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7212876281763406467L;

	public AntCycleVoid(ILsaNode node, ILsaMolecule molecule,
			IEnvironment<List<? extends ILsaMolecule>> env,
			double freq) {
		super(node, molecule, env, freq);
		// TODO Auto-generated constructor stub
	}
	
	public AntCycleVoid(ILsaNode node, ILsaMolecule molecule,
			IEnvironment<List<? extends ILsaMolecule>> env,
			double freq, boolean checkBatteryLevel) {
		super(node, molecule, env, freq, checkBatteryLevel);
		// TODO Auto-generated constructor stub
	}
	
	public AntCycleVoid(ILsaNode node, ILsaMolecule molecule,
			IEnvironment<List<? extends ILsaMolecule>> env, RandomEngine r,
			double awakeCons, double sleepCons, double minRad, double maxRad,
			double gain, double thres, double minProb, double maxProb,
			double sActiv, boolean checkBatteryLevel) {
		super(node, molecule, env, r, awakeCons, sleepCons, minRad, maxRad, gain,
				thres, minProb, maxProb, sActiv, checkBatteryLevel);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void behaviour() {
		// TODO Auto-generated method stub

	}

}
