package it.unibo.alchemist.model.implementations.actions;

import it.unibo.alchemist.external.cern.jet.random.engine.RandomEngine;
import it.unibo.alchemist.model.interfaces.IEnvironment;
import it.unibo.alchemist.model.interfaces.ILsaMolecule;
import it.unibo.alchemist.model.interfaces.ILsaNode;
import it.unibo.alchemist.model.interfaces.INode;
import it.unibo.alchemist.model.interfaces.IPosition;
import it.unibo.alchemist.model.interfaces.IReaction;

import java.util.List;

public class LsaFakeMove extends LsaRandomNeighborAction {

	private final IEnvironment<List<? extends ILsaMolecule>> env;
	private final RandomEngine r;

	public LsaFakeMove(final ILsaNode node, final ILsaMolecule molecule, final IEnvironment<List<? extends ILsaMolecule>> env, final RandomEngine r){
		super(node, molecule, env, r);
		this.env = env;
		this.r = r;
	}

	public IPosition getNextPosition() {
		return env.getPosition(getNode());
	}
	
	public LsaFakeMove cloneOnNewNode(final INode<List<? extends ILsaMolecule>> n, final IReaction<List<? extends ILsaMolecule>> r) {
		return new LsaFakeMove( (ILsaNode)n, getMolecule(), env, this.r);
	}

	@Override
	public void execute() {
		env.moveNodeToPosition(getNode(), getNextPosition());
	}


}
