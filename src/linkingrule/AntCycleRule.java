package it.unibo.alchemist.model.implementations.linkingrules;

import java.util.List;

import it.unibo.alchemist.model.implementations.molecules.LsaMolecule;
import it.unibo.alchemist.model.implementations.neighborhoods.Neighborhood;
import it.unibo.alchemist.model.interfaces.IEnvironment;
import it.unibo.alchemist.model.interfaces.ILinkingRule;
import it.unibo.alchemist.model.interfaces.ILsaMolecule;
import it.unibo.alchemist.model.interfaces.INeighborhood;
import it.unibo.alchemist.model.interfaces.INode;

public class AntCycleRule<T> implements ILinkingRule<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3849785549228964642L;

	@Override
	public INeighborhood<T> computeNeighborhood(INode<T> center,
			IEnvironment<T> env) {
		double range = 0;
		
		ILsaMolecule rMol = new LsaMolecule("radius, N");
		@SuppressWarnings("unchecked")
		List<ILsaMolecule> listRMol = (List<ILsaMolecule>) center.getConcentration(rMol);
		if(listRMol != null && listRMol.size() == 1){
			range = Double.parseDouble(listRMol.get(0).getArg(1).toString());
		}
		
		return new Neighborhood<>(center, env.getNodesWithinRange(center, range), env);
	}

}
