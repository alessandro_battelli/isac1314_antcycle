package it.unibo.alchemist.boundary.monitors;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.danilopianini.view.ExportForGUI;

import it.unibo.alchemist.boundary.interfaces.OutputMonitor;
import it.unibo.alchemist.model.implementations.molecules.LsaMolecule;
import it.unibo.alchemist.model.interfaces.IEnvironment;
import it.unibo.alchemist.model.interfaces.ILsaMolecule;
import it.unibo.alchemist.model.interfaces.INode;
import it.unibo.alchemist.model.interfaces.IReaction;
import it.unibo.alchemist.model.interfaces.ITime;
import it.unibo.alchemist.utils.L;

/**
 * @author Danilo Pianini
 * 
 * @param <T>
 */
@ExportInspector
public class AntCycleMonitor<T> implements OutputMonitor<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3277898733141785894L;
	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS", Locale.getDefault());
	private PrintStream writer;
	private String fpCache = null;
	private DecimalFormat df;

	@ExportForGUI(nameToExport = "File path")
	private String filePath = System.getProperty("user.home") + System.getProperty("file.separator") + "experiments" + System.getProperty("file.separator") + sdf.format(new Date()) + "-alchemist_report.log";
	@ExportForGUI(nameToExport = "Value separator")
	private String separator = ",";


	@Override
	public void finished(final IEnvironment<T> env, final ITime time, final long step) {
		if (writer != null) {
			writer.close();
		}
		writer = null;
		fpCache = null;
	}

	@Override
	public void initialized(final IEnvironment<T> env) {

	}



	@SuppressWarnings("unchecked")
	@Override
	public void stepDone(final IEnvironment<T> env, final IReaction<T> r, final ITime time, final long step) {

		if(time.toDouble() == 0){
			DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
			otherSymbols.setDecimalSeparator('.');
			df = new DecimalFormat("0.00000", otherSymbols);
			if (System.identityHashCode(fpCache) != System.identityHashCode(filePath)) {
				fpCache = filePath;
				try {
					writer = new PrintStream(new File(fpCache));
				} catch (FileNotFoundException e) {
					L.error(e);
				}
			}

			writer.println("'Steptime','Nodeid','Batterylvl','Activated','Light'");
		}

		int nodeNumber = env.getNodesNumber();

		if(time.toDouble() > 2880 && time.toDouble() <=4320 && (step+1)%nodeNumber == 0){

			double light = light(time.toDouble());

			for(INode<T> n : env.getNodes()){

				ILsaMolecule bMol = new LsaMolecule("battery, N");
				List<ILsaMolecule> listBMol = (List<ILsaMolecule>) n.getConcentration(bMol);
				double battery = Double.parseDouble(listBMol.get(0).getArg(1).toString());
				ILsaMolecule aMol = new LsaMolecule("active, N");
				List<ILsaMolecule> listAMol = (List<ILsaMolecule>) n.getConcentration(aMol);
				String active = listAMol.get(0).getArg(1).toString();
				writer.println(time.toString()+separator+n.getId()+separator+df.format(battery)+separator+active+separator+df.format(light));
			}

		}

	}


	private double light(double stepp){
		double step = stepp%1440;
		if(step < 420 || step > 1140){
			return 0;
		} else {
			return (1- Math.cos((step-420) * Math.PI /360))/2;
		}

	}	

}
